# Firejail

> Firejail is a program which allows you run another program in a sandbox
> by using Linux SUID permissions.

> Once Firejail is installed it has no settings for any programs.
> Firejail comes with numerous configuration files for various programs.
> The profiles are stored at `/etc/firejail` after the install. 
> You can use these profiles or create your own.
> You can start with the installed profiles and change these as needed.
> To do this you need to copy them to the folder `~/.config/firejail/`.
> https://www.linux.org/threads/sandboxing-with-firejail.27106/


Found what looks like a good guide:
https://github.com/alexjung/Run-Zoom-in-a-Sandbox


## Refs

+ https://ar.al/2020/06/25/how-to-use-the-zoom-malware-safely-on-linux-if-you-absolutely-have-to/
+ https://www.techrepublic.com/article/how-to-install-and-use-firejail-on-linux/
+ https://pvera.net/posts/apparmor-firejail-sandboxing/
+ https://www.linux.org/threads/sandboxing-with-firejail.27106/
+ https://forums.opensuse.org/showthread.php/540159-What-is-the-best-way-to-run-zoom-in-a-jail
+ https://wiki.archlinux.org/title/Firejail


+ https://github.com/netblue30/firejail/issues/3428
+ https://github.com/netblue30/firejail/issues/3482
+ https://github.com/netblue30/firejail/issues/3711
+ https://github.com/netblue30/firejail/issues/3726
+ https://github.com/netblue30/firejail/issues/3744
